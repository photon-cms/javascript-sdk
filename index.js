let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let port = 3000;

io.on('connection', function (socket) {
    socket.on('chat message', function (msg) {
        io.emit('chat message', msg);
    });
    socket.on('title', function (msg) {
        io.emit('title', msg);
    });
    socket.on('content', function (msg) {
        io.emit('content', msg);
    });
});

http.listen(port, function () {
    console.log('listening on *:' + port);
});

// const express = require("express");
// const https = require('https');
// const fs = require('fs');
//
// const expressApp = express();
//
// //HTTPS server
// const secureServer = https.createServer({
//     key: fs.readFileSync('ssl/server.key'),
//     cert: fs.readFileSync('ssl/server.crt'),
//     //requestCert: false,
//     //rejectUnauthorized: false
// }, expressApp);
//
// const ios = require('socket.io')(secureServer,{
//     cors: {
//         origin: "https://sportske-nove.test",
//         methods: ["GET", "POST"]
//     }
// });
//
// //Setup public file
// expressApp.use(express.static('public'))
//
// const HTTPS_PORT = 3000;
//
// /*
// ios.on("connection", (socket) => {
//     new Socket(socket)
// });
// */
//
// ios.on('connection', function(socket){
//     socket.on('chat message', function(msg){
//         io.emit('chat message', msg);
//     });
//     socket.on('title', function(msg){
//         io.emit('title', msg);
//     });
//     socket.on('content', function(msg){
//         io.emit('content', msg);
//     });
// });
//
// /*class Socket {
//     constructor(socket) {
//         console.log("connection from client");
//         this.socket = socket;
//         this.socket.on('message', this.onMessage.bind(this));
//         this.socket.on('title', function (msg) {
//             io.emit('title', msg);
//         });
//         this.socket.on('content', function (msg) {
//             io.emit('content', msg);
//         });
//     }
// }*/
//
// /*Socket.prototype.onMessage = function (msg) {
//     console.log("client: " + msg)
//     const fromServer = "Server: " + msg
//     console.log(fromServer);
//     this.socket.emit("message", fromServer);
// }*/
//
// secureServer.listen(HTTPS_PORT, () => {
//     console.log("secure server started at 3000");
// })
