# PHOTONCMS Javascript SDK

## Setup
Via npm package manager once it is published

## Initialization
In order to use the SDK first thing you need to do is to create an instance of `Client` using the user's email and password, or token if you have it and the API endpoint that you wish to fetch results from as arguments.
``` javascript
const PhotonClient = require('photoncms-javascript-sdk');
  let config = {
      baseUrl: 'http://photoncms.test/api',
      email: 'super.administrator@photoncms.test',
	  password: 'L!gthsp44d',
      token: 'eyJj0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly9waG90b25jbXMtc2Fhcy50ZXN0L2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTQ2OTM0MzQ2LCJleHAiOjE1NDcxMDcxNDYsIm5iZiI6MTU0NjkzNDM0NiwianRpIjoiSk1CUUVQQ1IxOWhmMVNvWiJ9.UCnmeZbXKU9mN_XQNuA1ttGkv9dT0DY1NuScf77l2Gc',
  }
let client = new PhotonClient(config);
```

Every response you recieve from API will be related to the user credentials you used, and will take into account their roles and permissions when retrieving data.

## Usage
* [Locales](#locales)
* [Included Fields](#included_fields)
* [Impersonate](#impersonate)
* [Model](#model)
* [Caching](#caching)
* [Calls](#calls)

### Locales
You can set up an array of locales that will be retreived via the API. You will receive translations in those locales from every translatable field, if translations exist. If you do not set locales the default locale that is setup in your Photon CMS project will be used. This method will return self, `Client` object, so you can chain methods.
``` javascript
client
	.setLocales(['en', 'de', 'fr'])
	.getAll(response => {
		...
	});
```
You can also check current locales set in your client using this method:
``` javascript
client.getLocales();
```
### Included Fields
You can set up an array of included fields that will be used for filtering response retreived via the API. Values of this array should be `fields` of the model you are working with. Only fields specified in this array will be retreived. If you do not set included fields the entire response will be returned. This method will return self, `Client` object, so you can chain methods.
``` javascript
client.setIncludedFields(['id', 'created_at', 'first_name', 'last_name'])
	.getAll(response => {
		...
	});
```
You can also check current included fields set in your client using this method:
``` javascript
client.getIncludedFields();
```
### Impersonate
If your instance is created with a user that has super administrator role you'll have an option of impersonating other users. This will allow you to recieve API responses like those users would, with their own roles and permissions taken into account. This method is asynchronous and return `Promise` with resolved same instance of `Client` so you can make your next api call.
``` javascript
# this method will impersonate user with specific id, and every subsequent request created with this client would be made in his name
client.impersonate($id)
	.then(client => {
		...
	})

# with this method you'll stop user impersonation and return to original super administrator user that initialized the client in the first place

client.stopImpersonate()
	.then(client => {
		...
	})
```
### Model
Before making any API calls you'll need to specify a model that you wish to access using a model name. Failing to do so will cause an exception. This method will return self, Client object, so you can chain methods.
``` javascript
# setting up model that will be used by the client
client.setModel('news')
	.getAll(response => {
		...
	});
# method to check which model is currently being used
client.getModel();
```
### Caching
SDK brings out of the box simple in-memory cache option. It is disabled by default. If you want to enable it all you need to do is to pass `cache: true` param to config object when you instantiate `Client`.
``` javascript
const PhotonClient = require('photoncms-javascript-sdk');
  let config = {
		baseUrl: 'http://photoncms.test/api',
		email: 'super.administrator@photoncms.test',
		password: 'L!gthsp44d',
		cache: true,
  }
let client = new PhotonClient(config);
```
This will enable caching responses for the following api calls:
* get()
* getAll()
* filter()
* count()

if you want to clear cache for specific model you can do that with the following method:
``` Javascript
clinet.setModel('news')
	.flushCache()
	.getAll(response => {
		...
	})
```
### Calls
#### Make API call
You can make any existing API call using this method.
``` javascript
# make call to API route without sending request body or additional header parameters
client.makeApiCall('get', 'existing/endpoint')
	.then(response => {
		...
	})

# make call to API route together with request body and additional header parameters
client.makeApiCall('post', 'existing/endpoint', requestBodyObject, requestHeadersObject)
	.then(response => {
		...
	})
```
#### Get all
This call will get all entries from a model.
``` javascript
client.setModel('news')
	.getAll()
	.then(response => {
		...
	})
```
#### Get single
This call will get a single entry from a model.
``` javascript
client.setModel('news')
	.get(id)
	.then(response => {
		...
	})
```
#### Create
This call will create a new entry for a model. If you have translatable field just need to pass object with locales key and translations values.
``` javascript
client.setModel('news')
	.create({
		title: {
			en: 'English News Title',
			fr: 'French News Title'
		},
		content: {
			en: 'Englesh content...',
			fr: 'French content...',
		},
		author: 23,
	})
	.then(response => {
		...
	})
```
#### Update
This call will update an existing entry for a model.
``` javascript
client.setModel('news')
	.update(entryId, {
		title: {
			en: 'Updated English News Title',
			fr: 'Updated French News Title'
		},
		content: {
			en: 'Updated Englesh content...',
			fr: 'UpdatedFrench content...',
		},
	})
	.then(response => {
		...
	})
```
#### Delete
This call will delete an entry from a model.
``` javascript
client.setModel('news')
	.delete(newsId)
	.then(response => {
		...
	});
```
#### Filter
This call will return a filtered list of entries from a model. Also this method provide pagination and sorting functionality.
``` javascript
client.setModel('news')
	.filter({
		filter: {
			newsCategoryId: {equals: 3},
			isPublished: {equals: true},
		},
		pagination: {
			items_per_page: 10,
			current_page: 1,
		},
		sorting: {
			publishedDate: 'desc',
		},
	})
	.then(response => {
		...
	})

```
#### Count
This call will return a count of entries from a model.
``` javascript
client.setModel('news')
	.count()
	.then(response => {
		...
	})
```
You can also count filtered entries:
``` javascript
client.setModel('news')
	.count({
		filter: {
			isPublished: {equals: true},
		}
	})
	.then(response => {
		...
	})
```

#### Creating Assets
##### Node.js
To create new asset you need to set model to `assets` which is system model and then use create method  with given file path relative to root project folder as a second parameter.

``` javascript
client.setModel('assets')
	.create({
		title: {
			en: 'Image tittle',
		},
		alt_text: {
			en: 'image alt text',
		}
	}, '/temp/profil_image.jpeg')
	.then((response) => {
		...
	});
```
#### Logout
This method will invalidate token, so in order to make api calls in the future, you will need to provide username and password again.
``` javascript
client.logout()
	.then((response) => {
		...
	});
```