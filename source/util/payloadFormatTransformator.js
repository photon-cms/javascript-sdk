jsonToFormData = function (item, newObject = {}, keyname = '', isFirstRecursionIteration = true) {
    if (typeof item === 'object') {
        for (const property in item) {
            let newKeyname = isFirstRecursionIteration ? `${keyname}${property}` : `${keyname}[${property}]`;
            jsonToFormData(item[property], newObject, newKeyname, false);
        }
    } else {
        newObject[keyname] = item;
    }

    return newObject;
};

exports.jsonToFormData = jsonToFormData;
