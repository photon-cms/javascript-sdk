const pThrottle = require('p-throttle');
const AuthService = require('./services/authService');
const FactoryService = require('./services/factoryService');
const Cache = require('./services/cacheService');

/**
 * Photon CMS Client Class.
 */
class Client {
    constructor(config) {
        this.authService = new AuthService(config);
        this.model = null;
        this.locales = null;
        this.includedFields = null;
        this.headers = Object.assign({}, config.headers);
        let rateLimit = config.rateLimit || 5;
        this.throttle = pThrottle(this.throttledRequest, rateLimit, 1000);
        this.requestService = FactoryService.getRequestService(config.baseUrl, config.timeout);
        this.shouldCache = config.cache ? true : false;
        this.cache = config.cache ? new Cache() : null;
    }

    /**
     * Gets a single model entry
     *
     * @param   {Integer}  id
     * @return  {Object}
     */
    get(id) {
        const model = this.getModel();
        const getRequest = this.requestService.getRequest;
        return this.throttle(getRequest, `${model}/${id}`, {}, null, null, this.shouldCache);
    }

    /**
     * Gets all model entries
     *
     * @return  {Object}
     */
    getAll() {
        const model = this.getModel();
        const getRequest = this.requestService.getRequest;
        return this.throttle(getRequest, model, {}, null, null, this.shouldCache);
    }

    /**
     * Gets the model property
     *
     * @return  {String}
     */
    getModel() {
        return this.model;
    }

    /**
     * Sets the model property.
     *
     * @param  {String}  token
     * @return  {Client}
     */
    setModel(model) {
        this.model = model;
        return this;
    }

    /**
     * Impersonate user.
     *
     * @param {Number} id
     * @return {Client};
     */
    async impersonate(id) {
        const getRequest = this.requestService.getRequest;
        let response = await this.throttle(getRequest, `/auth/impersonate/${id}`);
        if (response.message !== 'IMPERSONATING_ON') {
            throw response.message;
        }
        this.authService.setToken(response.body.token.token);

        return this;
    }

    /**
     * Stop impersonating user.
     *
     * @return {Client};
     */
    async stopImpersonate() {
        const getRequest = this.requestService.getRequest;
        let response = await this.throttle(getRequest, `/auth/impersonate/stop`);
        if (response.message !== 'IMPERSONATING_OFF') {
            throw response.message;
        }
        this.authService.setToken(response.body.token.token);

        return this;
    }

    /**
     * Set locales.
     *
     * @param {Array} locales
     * @return {Client};
     */
    setLocales(locales) {
        this.locales = locales;

        return this;
    }

    /**
     * Get locales.
     *
     * @return {Array};
     */
    getLocales() {
        return this.locales;
    }

    /**
     * Set included fields.
     *
     * @param {Array} includedFields
     * @return {Client};
     */
    setIncludedFields(includedFields) {
        this.includedFields = includedFields;
        return this;
    }

    /**
     * Get included fields.
     *
     * @return {Array};
     */
    getIncludedFields() {
        return this.includedFields;
    }

    /**
     * Create entry for model.
     *
     * @param   {Object}  payload
     * @return  {Object}
     */
    create(payload, filepath = null) {
        const model = this.getModel();
        const requestHandler =
            model === 'assets' ? this.requestService.postAssetRequest : this.requestService.postRequest;
        return this.throttle(requestHandler, model, payload, filepath);
    }

    /**
     * Update entry for model.
     *
     * @param   {Number}  entryId
     * @param	{Object} payload
     * @return  {Object}
     */
    update(entryId, payload) {
        const model = this.getModel();
        const url = `${model}/${entryId}`;
        const requestHandler = this.requestService.putRequest;
        return this.throttle(requestHandler, url, payload);
    }

    /**
     * Return a filtered list of entries from a model.
     *
     * @param	{Object} payload
     * @return  {Object}
     */
    filter(payload) {
        const model = this.getModel();
        const url = `filter/${model}`;
        const requestHandler = this.requestService.postRequest;
        return this.throttle(requestHandler, url, payload, null, null, this.shouldCache);
    }

    /**
     * Delete entry.
     *
     * @param	{Number} entryId
     * @param	{Object} options
     * @return  {Object}
     */
    delete(entryId, options = { force: false }) {
        const model = this.getModel();
        let force = options.force ? 1 : 0;
        const url = `${model}/${entryId}?force=${force}`;
        const requestHandler = this.requestService.deleteRequest;
        return this.throttle(requestHandler, url);
    }

    /**
     * Count all entries or filtered entries for model.
     *
     * @param	{Object} payload
     * @return  {Object}
     */
    count(payload = {}) {
        const model = this.getModel();
        const url = `count/${model}`;
        const requestHandler = this.requestService.postRequest;
        return this.throttle(requestHandler, url, payload, null, null, true);
    }

    /**
     * Delete all cache data for selected model
     *
     * @return  {Client}
     */
    flushCache() {
        this.cache.flush(this.model);
        return this;
    }

    /**
     * Make api call.
     *
     * @param	{string} method
     * @param	{string} endpoint
     * @param	{Object} payload
     * @param	{Object} headers
     * @return  {Object}
     */
    makeApiCall(method, endpoint, payload = {}, headers = {}) {
        let url = endpoint;
        const requestHandler = this.requestService[`${method}Request`];
        return this.throttle(requestHandler, url, payload, null, headers);
    }

    /**
     * Logout client, invalidates token
     */
    logout() {
        return this.makeApiCall('get', 'auth/logout');
    }

    /**
     * Generate included fields url suffix.
     *
     * @return {String};
     */
    generateIncludedFieldsSuffix() {
        if (!this.includedFields) {
            return '';
        }
        let urlSuffix = '?';
        this.includedFields.forEach((field) => {
            urlSuffix += `include[]=${field}&`;
        });
        return urlSuffix;
    }

    /**
     * Performs a throttled request
     *
     * @param   {String}  type
     * @param   {String}  url
     * @param   {Object}  params
     * @return  {Promise}
     */
    async throttledRequest(requestHandler, url, payload = {}, filepath = null, headers = null, isCache = false) {
        try {
            await this.authService.authenticate();

            url += this.generateIncludedFieldsSuffix();

            let token = this.authService.getToken();
            if (token) {
                this.headers = Object.assign(this.headers, { Authorization: `Bearer ${token}` });
            }

            if (this.locales) {
                let localesHeader = { Locales: this.locales.join(',') };
                this.headers = Object.assign(this.headers, localesHeader);
            }

            this.headers = headers ? { ...this.headers, ...headers } : this.headers;

            if (isCache) {
                let response = this.cache.get(this.model, url, payload);
                if (response) {
                    return response;
                }
            }

            let response = await requestHandler(url, this.headers, payload, filepath);
            if (isCache) {
                this.cache.add(this.model, url, payload, response.data);
            }
            return response.data;
        } catch (error) {
            console.log(error);
            if (error.response.data.message === 'TOKEN_EXPIRED' || error.response.data.message === 'TOKEN_INVALID') {
                this.authService.setToken(null);
                return this.throttledRequest(type, url, params);
            }
            throw error;
        }
    }
}

module.exports = Client;
