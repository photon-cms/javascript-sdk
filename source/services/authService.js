const axios = require('axios');

class AuthService {
    constructor(config) {
        this.token = config.token || null;
        this.email = config.email || null;
        this.password = config.password || null;
        this.loginRetryAttempts = false;
        this.client = axios.create({
            baseURL: config.baseUrl,
            timeout: config.timeout || 0,
        });
    }

    /**
     * Attempts to authenticate a user.
     *
     * @return  {Void}
     */
    async authenticate() {
        if (this.getToken()) {
            return false;
        }

        if (this.loginRetryAttempts) {
            console.log('maxRetryAttempts limit reached.');
            return;
        }
        this.loginRetryAttempts = true;
        let response = await this.client.post('/auth/login', { email: this.email, password: this.password });
        return this.setToken(response.data.body.token.token);
    }

    /**
     * Gets the token property
     *
     * @return  {String}
     */
    getToken() {
        return this.token;
    }

    /**
     * Sets the token and appropriate authorization header.
     *
     * @param  {String}  token
     * @return  {Client}
     */
    setToken(token) {
        this.token = token;
        return this;
    }
}

module.exports = AuthService;
