const qs = require('qs');

class Cache {
    constructor() {
        this.data = {};
    }

    /**
     * Save new key value pair to data cache object.
     *
     * @param {string} model
     * @param {string} url
     * @param {Object} payload
     * @param {Object} value
     * @return {void}
     */
    add = (model, url, payload, value) => {
        let key = this.generateKey(url, payload);
        if (!this.data[model]) {
            this.data[model] = {};
        }
        this.data[model][key] = value;
    };

    /**
     * Return cached value
     *
     * @param {string} model
     * @param {string} url
     * @param {Object} payload
     * @return {Object}
     */
    get = (model, url, payload) => {
        if (!this.data[model]) {
            return null;
        }
        let key = this.generateKey(url, payload);
        return this.data[model][key] ? this.data[model][key] : null;
    };

    /**
     * Delete all cached data related to given model (table name)
     *
     * @param {string} model
     * @return {void}
     */
    flush = (model) => {
        this.data[model] = null;
    };

    /**
     * Generate cache key from given request url and payload
     *
     * @param {string} endpoint
     * @param {Object} payload
     * @return {void}
     */
    generateKey = (endpoint, payload) => {
        let payloadString = qs.stringify(payload);
        return endpoint + payloadString;
    };
}

module.exports = Cache;
