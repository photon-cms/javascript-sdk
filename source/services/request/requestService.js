const axios = require('axios');

class RequestService {
    constructor(baseUrl, timeout) {
        this.baseUrl = baseUrl;
        this.timeout = timeout ? timeout : 0;
        this.axiosClient = axios.create({
            baseURL: this.baseUrl,
            timeout: this.timeout,
        });
        this.self = this;
    }

    getRequest = async (url, headers) => {
        let response = await this.axiosClient['get'](url, { headers });
        return response;
    };

    postRequest = async (url, headers, payload) => {
        let response = await this.axiosClient['post'](url, payload, { headers });
        return response;
    };

    putRequest = async (url, headers, payload) => {
        let response = await this.axiosClient['put'](url, payload, { headers });
        return response;
    };

    deleteRequest = async (url, headers) => {
        let response = await this.axiosClient['delete'](url, { headers });
        return response;
    };
}

module.exports = RequestService;
