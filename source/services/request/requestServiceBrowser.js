const RequestService = require('./requestService');

class BrowserRequestService extends RequestService {
    constructor(baseUrl, timeout) {
        super(baseUrl, timeout);
    }

    postAssetRequest = (url, headers, payload, filepath) => {
        //implement post asset request in browser environment
    };
}

module.exports = BrowserRequestService;
