const NodeRequestService = require('./request/requestServiceNode');
const BrowserServiceNode = require('./request/requestServiceBrowser');

const isNode = new Function('try {return this===global;}catch(e){return false;}');
const environment = isNode() ? 'node' : 'browser';

class FactoryService {
    constructor() {}

    static getRequestService(baseUrl, timeout) {
        return environment === 'node'
            ? new NodeRequestService(baseUrl, timeout)
            : new BrowserServiceNode(baseUrl, timeout);
    }

    static getCacheService() {
        //cache service factory
    }
}

module.exports = FactoryService;
